#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cstdarg>
#include <pthread.h>
#include <cassert>
#include <semaphore.h>
#include "common.h"

using namespace std;

const int MEM_PAGES = 1024;
const int DISK_PAGES = 1024;
// MEM_PAGES + extra 4KiB for alignment
uint8_t g_Memory[MEM_PAGES * CCPU::PAGE_SIZE + CCPU::PAGE_SIZE];
// align to a mutiple of 4KiB
uint8_t *g_MemoryAligned = (uint8_t *) ((((uintptr_t) g_Memory) + CCPU::PAGE_SIZE - 1) & ~(uintptr_t) ~CCPU::ADDR_MASK);
// swap file access
FILE *g_Fp;
// mutex for swap file (swap read/write functions are thread-safe)
pthread_mutex_t g_Mtx;

//-------------------------------------------------------------------------------------------------
static void seqTest1(CCPU *cpu,
                     void *arg) {
    for (uint32_t i = 0; i < 2000; i += 4) {
        uint32_t x;
        assert (cpu->readInt(i, x));
        assert (x == 0);
    }

    for (uint32_t i = 19230400; i < 20230400; i += 4) {
        uint32_t x;
        assert (cpu->readInt(i, x));
        assert (x == 0);
    }
}
//-------------------------------------------------------------------------------------------------
static void seqTest2(CCPU *cpu,
                     void *arg) {
    for (uint32_t i = 0; i < 2000; i += 4) {
        assert (cpu->writeInt(i, i + 1234567));
    }

    for (uint32_t i = 19230400; i < 20230400; i += 4) {
        assert (cpu->writeInt(i, i + 7654321));
    }

    for (uint32_t i = 0; i < 2000; i += 4) {
        uint32_t x;
        assert (cpu->readInt(i, x));
        assert (x == i + 1234567);
    }

    for (uint32_t i = 19230400; i < 20230400; i += 4) {
        uint32_t x;
        assert (cpu->readInt(i, x));
        assert (x == i + 7654321);
    }
}
//-------------------------------------------------------------------------------------------------
static void seqTest3(CCPU *cpu, void *arg) {
    for (uint32_t i = 0; i < *(uint32_t *) arg; i += 4) {
        assert (cpu->writeInt(i, i));
        assert (cpu->writeInt(i + *(uint32_t *) arg, i * 2));
    }
    for (uint32_t i = 0; i < *(uint32_t *) arg; i += 4) {
        uint32_t x;
        assert (cpu->readInt(i, x));
        assert(x == i);
        assert (cpu->readInt(i + *(uint32_t *) arg, x));
        assert(x == 2 * i);
    }
}
//-------------------------------------------------------------------------------------------------
static void seqTest4(CCPU *cpu, void *arg) {
    constexpr uint32_t numsPerPage = ((4096 * 8) / 32);
    // fill 12 pages
    for (uint32_t i = 0; i < numsPerPage * 12; i += 4) {
        assert(cpu->writeInt(i, i | 1));
    }
    // overwrite 3rd page
    for (uint32_t i = numsPerPage * 3; i < numsPerPage * 4; i += 4) {
        assert(cpu->writeInt(i, i & ~1));
    }
    // assert 7th page
    for (uint32_t i = numsPerPage * 7; i < numsPerPage * 8; i += 4) {
        uint32_t res;
        assert(cpu->readInt(i, res));
        assert(res == (i | 1));
    }
    // assert 3rd page
    for (uint32_t i = numsPerPage * 3; i < numsPerPage * 4; i += 4) {
        uint32_t res;
        assert(cpu->readInt(i, res));
        assert(res == (i & ~1));
    }
    uint32_t res;
    assert(cpu->readInt(0, res));
    assert(res == 1);
    assert(cpu->readInt(8 * numsPerPage + 4, res));
    assert(res == ((8 * numsPerPage + 4) | 1));

}
//-------------------------------------------------------------------------------------------------
static void seqTest5(CCPU *cpu, void *arg) {
    for (uint32_t i = 0; i < 2000000; i += 4) {
        assert (cpu->writeInt(i, i + 1234567));
    }
    for (uint32_t i = 0; i < 2000000; i += 4) {
        uint32_t x;
        assert (cpu->readInt(i, x));
        assert (x == i + 1234567);
    }
}
//-------------------------------------------------------------------------------------------------
static void parTest1(CCPU *cpu,
                     void *arg) {
    cpu->newProcess(nullptr, seqTest1);
    cpu->newProcess(nullptr, seqTest2);
    cpu->newProcess(nullptr, seqTest2);
}
//-------------------------------------------------------------------------------------------------
static void parTest2(CCPU *cpu, void *arg) {
    cpu->newProcess(nullptr, parTest1);
    cpu->newProcess(nullptr, seqTest1);
    cpu->newProcess(nullptr, seqTest2);
    cpu->newProcess(nullptr, seqTest2);
    cpu->newProcess(nullptr, seqTest4);
    cpu->newProcess(nullptr, seqTest5);
}
//-------------------------------------------------------------------------------------------------
bool fnReadPage(uint32_t memFrame,
                uint32_t diskPage) {
    pthread_mutex_lock(&g_Mtx);
    fseek(g_Fp, diskPage * CCPU::PAGE_SIZE, SEEK_SET);
    size_t read = fread(g_MemoryAligned + memFrame * CCPU::PAGE_SIZE, 1, CCPU::PAGE_SIZE, g_Fp);
    pthread_mutex_unlock(&g_Mtx);
    return read == CCPU::PAGE_SIZE;
}
//-------------------------------------------------------------------------------------------------
bool fnWritePage(uint32_t memFrame,
                 uint32_t diskPage) {
    pthread_mutex_lock(&g_Mtx);
    fseek(g_Fp, diskPage * CCPU::PAGE_SIZE, SEEK_SET);
    bool res = fwrite(g_MemoryAligned + memFrame * CCPU::PAGE_SIZE, 1, CCPU::PAGE_SIZE, g_Fp) == CCPU::PAGE_SIZE;
    pthread_mutex_unlock(&g_Mtx);
    return res;
}
//-------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {
    if (argc == 1)
        g_Fp = fopen("/tmp/pagefile", "w+b");
    else
        g_Fp = fopen(argv[1], "w+b");
    if (!g_Fp) {
        printf("Cannot create swap file\n");
        return 1;
    }
    pthread_mutex_init(&g_Mtx, nullptr);

    uint32_t t = 4100;
    memMgr(g_MemoryAligned, MEM_PAGES, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest1);
    memMgr(g_MemoryAligned, MEM_PAGES, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest2);
    memMgr(g_MemoryAligned, MEM_PAGES, DISK_PAGES, fnReadPage, fnWritePage, &t, seqTest3);
    memMgr(g_MemoryAligned, MEM_PAGES, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest4);
    memMgr(g_MemoryAligned, MEM_PAGES, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest5);

    for (int i = 4; i < 100; i++) {
        memMgr(g_MemoryAligned, i, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest1);
        memMgr(g_MemoryAligned, i, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest2);
        memMgr(g_MemoryAligned, i, DISK_PAGES, fnReadPage, fnWritePage, &t, seqTest3);
        memMgr(g_MemoryAligned, i, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest4);
        memMgr(g_MemoryAligned, i, DISK_PAGES, fnReadPage, fnWritePage, nullptr, seqTest5);
    }

    // parTests
    memMgr(g_MemoryAligned, MEM_PAGES, DISK_PAGES, fnReadPage, fnWritePage, nullptr, parTest1);
    memMgr(g_MemoryAligned, 100, DISK_PAGES, fnReadPage, fnWritePage, nullptr, parTest1);
    memMgr(g_MemoryAligned, 100, DISK_PAGES+200, fnReadPage, fnWritePage, nullptr, parTest2);

    pthread_mutex_destroy(&g_Mtx);
    fclose(g_Fp);
    return 0;
}
