#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include "common.h"
#include <cassert>

using namespace std;
#endif /* __PROGTEST__ */

//----------------------------------------------------------------------------------------------------------------------

uint8_t *memPagesArray;
uint32_t memPagesMax;
pthread_mutex_t memPagesMutex;

uint8_t *diskPagesArray;
uint32_t diskPagesMax;
pthread_mutex_t diskPagesMutex;

uint32_t **memFramesToPTEs;
pthread_mutex_t memFramesToPTEsMutex;

uint32_t swapIndex;
pthread_mutex_t swapIndexMutex;

uint32_t runningThreads;
pthread_cond_t runningThreadsCond;
pthread_mutex_t runningThreadsMutex;

//----------------------------------------------------------------------------------------------------------------------

void printPageArray(const uint8_t *array, uint32_t size) {
    for (uint32_t i = 0; i < size; i++) {
        if ((array[i] & 0x1) == 0x1)
            printf("1");
        else
            printf("0");
    }
    printf("\n");
}

//----------------------------------------------------------------------------------------------------------------------

void freeProcess(uint8_t *m_MemStart, uint32_t m_PageTableRoot) {
    pthread_mutex_lock(&memPagesMutex);
    pthread_mutex_lock(&diskPagesMutex);

    auto *rootTable = (uint32_t *) (m_MemStart + (m_PageTableRoot & CCPU::ADDR_MASK));
    for (uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i++) {
        if ((rootTable[i] & CCPU::BIT_PRESENT) == CCPU::BIT_PRESENT) {
            auto *pageTable = (uint32_t *) (m_MemStart + (rootTable[i] & CCPU::ADDR_MASK));
            for (uint32_t j = 0; j < CCPU::PAGE_DIR_ENTRIES; j++) {
                if ((pageTable[j] & CCPU::BIT_PRESENT) == CCPU::BIT_PRESENT) {
                    memPagesArray[(pageTable[j] >> 12)] = 0;
                } else if (pageTable[j] != 0) {
                    diskPagesArray[(pageTable[j] >> 3)] = 0;
                }
            }
            memPagesArray[(rootTable[i] >> 12)] = 0;
        }
    }
    memPagesArray[(m_PageTableRoot >> 12)] = 0;

    pthread_mutex_unlock(&diskPagesMutex);
    pthread_mutex_unlock(&memPagesMutex);
}

//----------------------------------------------------------------------------------------------------------------------

struct threadStartArgs {
    CCPU *ccpu;
    void *params;

    void (*entryPoint)(CCPU *, void *);

    uint8_t *memStart;
    uint32_t rootTable;
};
//----------------------------------------------------------------------------------------------------------------------

void *threadStart(void *params) {

    auto *threadStartParams = (threadStartArgs *) params;

    threadStartParams->entryPoint(threadStartParams->ccpu, threadStartParams->params);

    freeProcess(threadStartParams->memStart, threadStartParams->rootTable);

    delete threadStartParams->ccpu;
    delete threadStartParams;

    pthread_mutex_lock(&runningThreadsMutex);
    runningThreads--;
    pthread_cond_signal(&runningThreadsCond);
    pthread_mutex_unlock(&runningThreadsMutex);

    return nullptr;
}

//----------------------------------------------------------------------------------------------------------------------

class CPU : public CCPU {
public:
    CPU(uint8_t *memStart, uint32_t pageTableRoot, bool(*readPage)(uint32_t memFrame, uint32_t diskPage),
        bool (*writePage)(uint32_t memFrame, uint32_t diskPage))
            : CCPU(memStart, pageTableRoot),
              readPage(readPage),
              writePage(writePage) {
    }

    bool newProcess(void *processArg, void (*entryPoint)(CCPU *, void *)) override {

        pthread_mutex_lock(&runningThreadsMutex);
        if (runningThreads > PROCESS_MAX) {
            pthread_mutex_unlock(&runningThreadsMutex);
            return false;
        }
        pthread_mutex_unlock(&runningThreadsMutex);

        pthread_mutex_lock(&memPagesMutex);
        pthread_mutex_lock(&diskPagesMutex);
        pthread_mutex_lock(&memFramesToPTEsMutex);

        int rootTablePage = findFreeMemFrame();

        if (rootTablePage == -1) {
            if (!swapPageToDisk()) {
                pthread_mutex_unlock(&memFramesToPTEsMutex);
                pthread_mutex_unlock(&diskPagesMutex);
                pthread_mutex_unlock(&memPagesMutex);
                return false;
            }
            rootTablePage = findFreeMemFrame();
        }

        memPagesArray[rootTablePage] = FRAME_BIT_TAKEN | FRAME_BIT_REFERENCE;

        pthread_mutex_unlock(&memFramesToPTEsMutex);
        pthread_mutex_unlock(&diskPagesMutex);
        pthread_mutex_unlock(&memPagesMutex);

        memset(m_MemStart + (rootTablePage << 12), 0, PAGE_SIZE);
        CPU *cpu = new CPU(m_MemStart, rootTablePage << 12, readPage, writePage);

        auto *threadStartArgs = new struct threadStartArgs();
        threadStartArgs->ccpu = cpu;
        threadStartArgs->params = processArg;
        threadStartArgs->entryPoint = entryPoint;
        threadStartArgs->memStart = m_MemStart;
        threadStartArgs->rootTable = (rootTablePage << 12);

        pthread_mutex_lock(&runningThreadsMutex);
        runningThreads++;
        pthread_mutex_unlock(&runningThreadsMutex);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_t process;
        pthread_create(&process, &attr, threadStart, (void *) threadStartArgs);
        pthread_attr_destroy(&attr);

        return true;
    }
    //------------------------------------------------------------------------------------------------------------------
protected:
    bool pageFaultHandler(uint32_t address, bool write) override {

        pthread_mutex_lock(&memPagesMutex);
        pthread_mutex_lock(&diskPagesMutex);
        pthread_mutex_lock(&memFramesToPTEsMutex);

        int frameNumber = findFreeMemFrame();

        // neni volny frame
        if (frameNumber == -1) {

            if (!swapPageToDisk()) {
                pthread_mutex_unlock(&memFramesToPTEsMutex);
                pthread_mutex_unlock(&diskPagesMutex);
                pthread_mutex_unlock(&memPagesMutex);
                return false;
            }

            pthread_mutex_unlock(&memFramesToPTEsMutex);
            pthread_mutex_unlock(&diskPagesMutex);
            pthread_mutex_unlock(&memPagesMutex);
            return true;
        }

        auto *rootTable = (uint32_t *) (m_MemStart + (m_PageTableRoot & ADDR_MASK));
        uint32_t rootTableIndex = (address >> 22);

        // zaznam v root tabulce neexistuje -> vytvorit novy frame
        if (rootTable[rootTableIndex] == 0) {

            memPagesArray[frameNumber] = FRAME_BIT_TAKEN;

            uint32_t newTableEntry = BIT_USER | BIT_WRITE | BIT_PRESENT | BIT_REFERENCED | (frameNumber << 12);
            rootTable[rootTableIndex] = newTableEntry;

            memset(m_MemStart + (newTableEntry & ADDR_MASK), 0, PAGE_SIZE);

            memFramesToPTEs[frameNumber] = rootTable + rootTableIndex;

            pthread_mutex_unlock(&memFramesToPTEsMutex);
            pthread_mutex_unlock(&diskPagesMutex);
            pthread_mutex_unlock(&memPagesMutex);
            return true;
        }

            // zaznam v root tabulce existuje, ale stranka je odswapovana na disk
            // tohle se nikdy nestane -> tabulky se nedaji odswapovat na disk
        else if ((rootTable[rootTableIndex] & BIT_PRESENT) == 0) {

            uint32_t diskFrame = rootTable[rootTableIndex] >> 3;

            readPage(frameNumber, diskFrame);

            diskPagesArray[diskFrame] &= ~FRAME_BIT_TAKEN;
            memPagesArray[frameNumber] |= FRAME_BIT_TAKEN;

            uint32_t newTableEntry = BIT_USER | BIT_WRITE | BIT_PRESENT | BIT_REFERENCED | (frameNumber << 12);
            rootTable[rootTableIndex] = newTableEntry;

            memFramesToPTEs[frameNumber] = rootTable + rootTableIndex;

            pthread_mutex_unlock(&memFramesToPTEsMutex);
            pthread_mutex_unlock(&diskPagesMutex);
            pthread_mutex_unlock(&memPagesMutex);
            return true;
        }

        uint32_t pageTableIndex = (address >> 12) & 0x3FF;
        auto *pageTable = (uint32_t *) (m_MemStart + (rootTable[rootTableIndex] & ADDR_MASK));

        // zaznam v tabulce neexistuje -> ziskat si novy frame
        if (pageTable[pageTableIndex] == 0) {

            memPagesArray[frameNumber] = FRAME_BIT_TAKEN | FRAME_BIT_SWAPPABLE | FRAME_BIT_REFERENCE;

            uint32_t newTableEntry = BIT_USER | BIT_WRITE | BIT_PRESENT | BIT_REFERENCED | (frameNumber << 12);
            pageTable[pageTableIndex] = newTableEntry;

            memset(m_MemStart + (newTableEntry & ADDR_MASK), 0, PAGE_SIZE);

            memFramesToPTEs[frameNumber] = pageTable + pageTableIndex;

            pthread_mutex_unlock(&memFramesToPTEsMutex);
            pthread_mutex_unlock(&diskPagesMutex);
            pthread_mutex_unlock(&memPagesMutex);
            return true;
        }

        // ramec je odlozeny na disku
        if ((pageTable[pageTableIndex] & BIT_PRESENT) == 0) {

            uint32_t diskFrame = pageTable[pageTableIndex] >> 3;

            readPage(frameNumber, diskFrame);
            diskPagesArray[diskFrame] &= ~FRAME_BIT_TAKEN;
            memPagesArray[frameNumber] = FRAME_BIT_TAKEN | FRAME_BIT_SWAPPABLE | FRAME_BIT_REFERENCE;

            uint32_t newTableEntry = BIT_USER | BIT_WRITE | BIT_PRESENT | BIT_REFERENCED | (frameNumber << 12);
            pageTable[pageTableIndex] = newTableEntry;

            memFramesToPTEs[frameNumber] = pageTable + pageTableIndex;

            pthread_mutex_unlock(&memFramesToPTEsMutex);
            pthread_mutex_unlock(&diskPagesMutex);
            pthread_mutex_unlock(&memPagesMutex);
            return true;
        }

        pthread_mutex_unlock(&memFramesToPTEsMutex);
        pthread_mutex_unlock(&diskPagesMutex);
        pthread_mutex_unlock(&memPagesMutex);
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    int findFreeDiskFrame() const {
        for (uint32_t i = 0; i < diskPagesMax; i++) {
            if ((diskPagesArray[i] & FRAME_BIT_TAKEN) == 0)
                return i;
        }
        return -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    int findFreeMemFrame() const {
        for (uint32_t i = 0; i < memPagesMax; i++) {
            if ((memPagesArray[i] & FRAME_BIT_TAKEN) == 0)
                return i;
        }
        return -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    unsigned int findFrameToSwap() const {
        pthread_mutex_lock(&swapIndexMutex);

        while (true) {
            if (swapIndex == memPagesMax)
                swapIndex = 1;

            if (((memPagesArray[swapIndex] & FRAME_BIT_TAKEN) == 0) ||
                ((memPagesArray[swapIndex] & FRAME_BIT_SWAPPABLE) == 0)) {
                swapIndex++;
                continue;
            }
            if (( *(memFramesToPTEs[swapIndex]) & BIT_REFERENCED) == 0) {
                int res = swapIndex++;
                pthread_mutex_unlock(&swapIndexMutex);
                return res;
            } else {
                *(memFramesToPTEs[swapIndex]) &= ~BIT_REFERENCED;
                swapIndex++;
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    bool swapPageToDisk() {

        uint32_t frameToSwap = findFrameToSwap();
        int freeDiskFrame = findFreeDiskFrame();

        if (freeDiskFrame == -1) {
            printf("Dosla veskera pamet.\n");
            return false;
        }

        if ((*(memFramesToPTEs[frameToSwap]) & BIT_DIRTY) == 0) {
            memPagesArray[frameToSwap] &= ~FRAME_BIT_TAKEN;
            *(memFramesToPTEs[frameToSwap]) = 0;
            memFramesToPTEs[frameToSwap] = nullptr;
            return true;
        }

        if (!writePage(frameToSwap, freeDiskFrame))
            return false;

        diskPagesArray[freeDiskFrame] |= FRAME_BIT_TAKEN;
        memPagesArray[frameToSwap] &= ~FRAME_BIT_TAKEN;

        uint32_t newTableEntry = BIT_WRITE | (freeDiskFrame << 3);
        *(memFramesToPTEs[frameToSwap]) = newTableEntry;

        memFramesToPTEs[frameToSwap] = nullptr;

        memset(m_MemStart + (frameToSwap << 12), 0, PAGE_SIZE);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------
private:
    bool (*readPage)(uint32_t memFrame, uint32_t diskPage);

    bool (*writePage)(uint32_t memFrame, uint32_t diskPage);

    uint8_t FRAME_BIT_TAKEN = 0x1;
    uint8_t FRAME_BIT_REFERENCE = 0x2;
    uint8_t FRAME_BIT_SWAPPABLE = 0x4;
};

//----------------------------------------------------------------------------------------------------------------------

void memMgr(void *mem,
            uint32_t memPages,
            uint32_t diskPages,
            bool           (*readPage)(uint32_t memFrame, uint32_t diskPage),
            bool           (*writePage)(uint32_t memFrame, uint32_t diskPage),
            void *processArg,
            void           (*mainProcess)(CCPU *, void *)) {

    pthread_cond_init(&runningThreadsCond, nullptr);

    pthread_mutex_init(&runningThreadsMutex, nullptr);
    pthread_mutex_init(&diskPagesMutex, nullptr);
    pthread_mutex_init(&memPagesMutex, nullptr);
    pthread_mutex_init(&memFramesToPTEsMutex, nullptr);
    pthread_mutex_init(&swapIndexMutex, nullptr);

    memPagesArray = new uint8_t[memPages];
    diskPagesArray = new uint8_t[diskPages];
    memFramesToPTEs = new uint32_t *[memPages];

    for (uint32_t i = 0; i < diskPages; i++)
        diskPagesArray[i] = 0;
    for (uint32_t i = 0; i < memPages; i++) {
        memPagesArray[i] = 0;
        memFramesToPTEs[i] = nullptr;
    }
    swapIndex = 1;

    memPagesMax = memPages;
    diskPagesMax = diskPages;

    runningThreads = 0;

    memset(mem, 0, memPages * CPU::PAGE_SIZE);

    auto *cpu = new CPU((uint8_t *) mem, 0, readPage, writePage);
    mainProcess(cpu, processArg);

    pthread_mutex_lock(&runningThreadsMutex);
    while (runningThreads != 0) pthread_cond_wait(&runningThreadsCond, &runningThreadsMutex);
    pthread_mutex_unlock(&runningThreadsMutex);

    pthread_mutex_destroy(&diskPagesMutex);
    pthread_mutex_destroy(&memPagesMutex);
    pthread_mutex_destroy(&runningThreadsMutex);
    pthread_mutex_destroy(&swapIndexMutex);
    pthread_mutex_destroy(&memFramesToPTEsMutex);

    pthread_cond_destroy(&runningThreadsCond);

    delete[] memPagesArray;
    delete[] diskPagesArray;
    delete[] memFramesToPTEs;
    delete cpu;
}
