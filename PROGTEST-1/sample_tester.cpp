#include <cstdlib>
#include <cstdio>
#include <chrono>
#include <thread>
#include <stdexcept>
#include "sample_tester.h"

using namespace std;

#include <random>
//=============================================================================================================================================================
void random_sleep(void) {
    std::mt19937_64 eng{std::random_device{}()}; // or seed however you want
    std::uniform_int_distribution<> dist{10, 15};
    std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});
}

//=============================================================================================================================================================
ASheet CProductionLineTest::getSheet(void) {
    size_t idx = m_GetPos++;
    if (idx == g_Sheets.size())
        return ASheet();
    if (idx > g_Sheets.size())
        throw std::invalid_argument("getSheet: called too many times");

    ASheet res = g_Sheets[idx];

    for (auto[relDev, x] : g_RefRelDev[idx])
        res->m_RelDev.emplace_back(relDev, 0);
    for (auto[volume, x] : g_RefVolume[idx])
        res->m_Volume.emplace(volume, 0);
    for (auto[range, x] : g_RefMinMax[idx])
        res->m_MinMax.emplace(range, 0);

    //random_sleep();
    return res;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
void CProductionLineTest::doneSheet(ASheet sheet) {
    size_t idx = m_DonePos++;
    //random_sleep();

    if (idx >= g_Sheets.size())
        throw std::invalid_argument("doneSheet: called too many times");

    if (g_Sheets[idx] != sheet)
        throw std::invalid_argument("doneSheet: order not preserved");

    if (sheet->m_RelDev != g_RefRelDev[idx])
        throw std::invalid_argument("doneSheet: invalid result (relDev)");

    if (sheet->m_Volume != g_RefVolume[idx])
        throw std::invalid_argument("doneSheet: invalid result (volume)");

    if (sheet->m_MinMax != g_RefMinMax[idx])
        throw std::invalid_argument("doneSheet: invalid result (minMax)");
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
bool CProductionLineTest::allProcessed(void) const {
    return m_GetPos == 1 + g_Sheets.size() && m_DonePos == g_Sheets.size();
}
//=============================================================================================================================================================
