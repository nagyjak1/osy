#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <string>
#include <vector>
#include <array>
#include <iterator>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <stdexcept>
#include <condition_variable>
#include <pthread.h>
#include <semaphore.h>
#include "progtest_solver.h"
#include "sample_tester.h"

using namespace std;
#endif /* __PROGTEST__ */

//----------------------------------------------------------------------------------------------------------------------

class CSheetWrapper;

atomic_int registeredLinesCnt;
atomic_int finishedLinesCnt;

deque<shared_ptr<CSheetWrapper>> solvingSheetsQueue;
mutex solvingSheetsQueueMtx;
condition_variable solvingSheetsQueueFull;
condition_variable solvingSheetsQueueEmpty;

const int MAX_LINE_QUEUE_SIZE = 200;
const int MAX_GLOBAL_QUEUE_SIZE = 400;

//----------------------------------------------------------------------------------------------------------------------

class CProductionLineWrapper;

class CSheetWrapper {
public:
    explicit CSheetWrapper(ASheet sh, CProductionLineWrapper *line)
            : sheet(move(sh)),
              productionLine(line) {}

    //------------------------------------------------------------------------------------------------------------------
    void solveSheet() {
        int **array = new int *[sheet->m_Length];
        for (int l = 0; l < sheet->m_Length; l++) {
            array[l] = new int[sheet->m_Width];
        }
        for (int q = 0; q < sheet->m_Length; q++) {
            for (int h = 0; h < sheet->m_Width; h++)
                array[q][h] = sheet->m_Thickness[q * sheet->m_Width + h];
        }
        for (auto &j : sheet->m_RelDev)
            sheet->updateRelDev(j.first, maxRectByRelDev(array, sheet->m_Width, sheet->m_Length, j.first));

        for (auto &j : sheet->m_MinMax)
            sheet->updateMinMax(j.first,
                                maxRectByMinMax(array, sheet->m_Width, sheet->m_Length, j.first.m_Lo, j.first.m_Hi));

        for (auto &j : sheet->m_Volume)
            sheet->updateVolume(j.first, maxRectByVolume(array, sheet->m_Width, sheet->m_Length, j.first));

        for (int l = 0; l < sheet->m_Length; l++)
            free(array[l]);
        free(array);
        done = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    ASheet sheet;
    atomic_bool done = false;
    CProductionLineWrapper *productionLine;
};

//----------------------------------------------------------------------------------------------------------------------

class CProductionLineWrapper {
public:
    explicit CProductionLineWrapper(AProductionLine line) {
        productionLine = move(line);
    }

    //------------------------------------------------------------------------------------------------------------------
    void producingThreadFnc() {
        while (true) {

            auto sheet = productionLine->getSheet();

            unique_lock<mutex> lock_queue(sheetsQueueMtx);
            sheetsQueueFull.wait(lock_queue, [&]() { return sheetsQueue.size() != MAX_LINE_QUEUE_SIZE; });

            if (!sheet) {
                sheetsQueue.push_back(nullptr);
                sheetsQueueEmptyOrNotSolved.notify_one();
                return;
            }
            auto tmp = make_shared<CSheetWrapper>(sheet, this);

            unique_lock<mutex> lock_global_queue(solvingSheetsQueueMtx);
            solvingSheetsQueueFull.wait(lock_global_queue,
                                        []() { return solvingSheetsQueue.size() != MAX_GLOBAL_QUEUE_SIZE; });
            solvingSheetsQueue.push_back(tmp);
            solvingSheetsQueueEmpty.notify_all();
            lock_global_queue.unlock();

            sheetsQueue.push_back(tmp);
            sheetsQueueEmptyOrNotSolved.notify_one();
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    void finishingThreadFnc() {
        while (true) {

            unique_lock<mutex> lock(sheetsQueueMtx);
            sheetsQueueEmptyOrNotSolved.wait(lock, [&]() {
                return !sheetsQueue.empty() && (sheetsQueue.front() == nullptr || sheetsQueue.front()->done);
            });

            while (!sheetsQueue.empty() && (sheetsQueue.front() == nullptr || sheetsQueue.front()->done)) {

                if (sheetsQueue.front() == nullptr) {
                    finishedLinesCnt++;
                    solvingSheetsQueueEmpty.notify_all();
                    return;
                }

                auto sheetPtr = sheetsQueue.front();
                sheetPtr->productionLine->productionLine->doneSheet(sheetPtr->sheet);
                sheetsQueue.pop_front();
                sheetsQueueFull.notify_one();
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    static void workingThreadFnc() {
        while (true) {

            unique_lock<mutex> lock(solvingSheetsQueueMtx);
            solvingSheetsQueueEmpty.wait(lock, []() {
                return !solvingSheetsQueue.empty() || finishedLinesCnt == registeredLinesCnt;
            });

            if (finishedLinesCnt == registeredLinesCnt) {
                solvingSheetsQueueEmpty.notify_all();
                return;
            }

            auto sheet = solvingSheetsQueue.front();
            solvingSheetsQueue.pop_front();
            solvingSheetsQueueFull.notify_one();
            lock.unlock();

            sheet->solveSheet();

            sheet->productionLine->sheetsQueueMtx.lock();
            sheet->productionLine->sheetsQueueEmptyOrNotSolved.notify_one();
            sheet->productionLine->sheetsQueueMtx.unlock();
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    AProductionLine productionLine;
    deque<shared_ptr<CSheetWrapper>> sheetsQueue;
    mutex sheetsQueueMtx;
    condition_variable sheetsQueueFull;
    condition_variable sheetsQueueEmptyOrNotSolved;
    thread producingThread;
    thread consumingThread;
};

//----------------------------------------------------------------------------------------------------------------------

class CQualityControl {
public:
    CQualityControl() {
        registeredLinesCnt = 0;
        finishedLinesCnt = 0;
        solvingSheetsQueue.clear();
    }

    //------------------------------------------------------------------------------------------------------------------
    static void checkAlgorithm(ASheet sheet) {
        CSheetWrapper(move(sheet), nullptr).solveSheet();
    }

    //------------------------------------------------------------------------------------------------------------------
    void addLine(AProductionLine line) {
        productionLines.emplace_back(move(line));
        registeredLinesCnt++;
    }

    //------------------------------------------------------------------------------------------------------------------
    void start(int workThreads) {
        for (auto &productionLine : productionLines)
            productionLine.producingThread = thread(&CProductionLineWrapper::producingThreadFnc, ref(productionLine));

        for (auto &productionLine : productionLines)
            productionLine.consumingThread = thread(&CProductionLineWrapper::finishingThreadFnc, ref(productionLine));

        for (int i = 0; i < workThreads; ++i)
            workingThreads.emplace_back(&CProductionLineWrapper::workingThreadFnc);
    }

    //------------------------------------------------------------------------------------------------------------------
    void stop() {
        for (auto &productionLine : productionLines) {
            productionLine.producingThread.join();
            productionLine.consumingThread.join();
        }
        for (auto &i : workingThreads)
            i.join();
    }

    //------------------------------------------------------------------------------------------------------------------
    deque<CProductionLineWrapper> productionLines;
    vector<thread> workingThreads;
};
//----------------------------------------------------------------------------------------------------------------------
int main() {
    CQualityControl q;
    AProductionLineTest line;
    q.addLine( line );
    q.start(4);
    q.stop();
    if ( line)

    return 0;
}